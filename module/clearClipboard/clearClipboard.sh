#!/bin/bash

cliphist wipe
exit_1=$?

wl-copy -c
exit_2=$?

wl-copy -cp
exit_3=$?

[[ -f ~/.cache/cliphist/db ]] && rm ~/.cache/cliphist/db
[[ -d ~/.cache/cliphist/ ]] && rmdir ~/.cache/cliphist/

if [ $exit_1 -ne 0 ] || [ $exit_2 -ne 0 ] || [ $exit_3 -ne 0 ]
then
  notify-send 'Clipboard Cleaner' "Something went wrong!" --icon=$(dirname -- ${BASH_SOURCE[0]})/dialog-error-symbolic.svg
else
  notify-send 'Clipboard Cleaner' "Clipboard cleared!" --icon=$(dirname -- ${BASH_SOURCE[0]})/clipboard.svg
fi
