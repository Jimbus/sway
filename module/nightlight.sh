#!/bin/bash

dim_brightness () {
	ddcutil --display 1 setvcp 10 24 # 150 Lux
	ddcutil --display 2 setvcp 10 17 # 150 Lux
}

reset_brightness () {
	SECONDS=0
	while [ $(ddcutil --display 1 getvcp 10 | awk '{ print substr($9, 1, length($9)-1) }') -ne 50 ]
	do
		ddcutil --display 1 setvcp 10 50 # 250 Lux

		if [ $SECONDS -eq 90 ]
		then
			echo "An issue occurred while restoring the brightness to its default setting"
			break
		fi
	done

	SECONDS=0
	while [ $(ddcutil --display 2 getvcp 10 | awk '{ print substr($9, 1, length($9)-1) }') -ne 42 ]
	do
		ddcutil --display 2 setvcp 10 42 # 250 Lux

		if [ $SECONDS -eq 90 ]
		then
			echo "An issue occurred while restoring the brightness to its default setting"
			break
		fi
	done
}

clean_exit () {
	pkill wlsunset
}

trap clean_exit SIGHUP SIGINT SIGQUIT SIGABRT SIGALRM SIGTERM SIGSTOP SIGTSTP SIGUSR1 SIGUSR2

TEST=$(ps x | awk '$5 ~ /wlsunset/ {print $5}')
if [ -z $TEST ]
then
	wlsunset -T 4050 &
	PID=$!

	dim_brightness

	while ps -p $PID > /dev/null
	do
		wait $PID
	done

	reset_brightness

	exit 0
else
	clean_exit
fi

