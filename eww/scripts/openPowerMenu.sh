#/bin/bash

isPowerMenuShown=$(eww -c ~/.config/sway/eww get isPowerMenuShown)
focussedMonitor=$(swaymsg -rt get_outputs | jq -r '.[0]|try select(.focused == true)')

if [ $isPowerMenuShown == "false" ]
then
  eww -c ~/.config/sway/eww close powerMenu

  if [ -z "$focussedMonitor" ]
  then 
    eww -c ~/.config/sway/eww open powerMenu --screen 1
  else 
    eww -c ~/.config/sway/eww open powerMenu --screen 0
  fi

  eww -c ~/.config/sway/eww update isPowerMenuShown=true
else
  eww -c ~/.config/sway/eww update isPowerMenuShown=false
fi
